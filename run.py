#!/usr/fed_client/env python
"""The run script"""
import logging
import subprocess
import sys
import zipfile
from pathlib import Path

import tensorflow as tf
from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_nvflare_client.main import format_output, start_client
from fw_gear_nvflare_client.utils import get_api_key

log = logging.getLogger(__name__)


CLIENT_DIR = Path(__file__).parent / "client"
if not CLIENT_DIR.exists():
    CLIENT_DIR.mkdir(parents=True)


def main(context: GearToolkitContext) -> None:
    """Parses config and run."""

    gpu_devices = tf.config.list_physical_devices("GPU")
    if gpu_devices:
        log.info(f"GPU available:\n{gpu_devices}")

    log.info("Unzipping client...")
    with zipfile.ZipFile(context.get_input_path("fed_client_zip"), "r") as zf:
        zf.extractall(str(CLIENT_DIR))

    log.info("Starting client...")
    api_key = get_api_key(context.config_json)
    destination = context.get_destination_parent()
    if destination.container_type != "project":
        log.error(
            f"The gear must be launch at the project level. "
            f"{destination.container_type} found"
        )
        sys.exit(1)
    else:
        project_lu = f"{destination.group}/{destination.label}"
    e_code = start_client(CLIENT_DIR, project_lu, api_key)

    if e_code == 0:
        log.info("Formatting output...")
        format_output(CLIENT_DIR, context.output_dir)

    sys.exit(e_code)


if __name__ == "__main__":  # pragma: no cover
    with GearToolkitContext() as gear_context:
        gear_context.init_logging()
        main(gear_context)
