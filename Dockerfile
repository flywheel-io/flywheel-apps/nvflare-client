FROM pytorch/pytorch:1.9.1-cuda11.1-cudnn8-runtime

ENV FLYWHEEL="/flywheel/v0"
WORKDIR ${FLYWHEEL}

# Dev install. git for pip editable install.
# hadolint ignore=DL3008
RUN apt-get update && apt-get install -y git && \
    rm -rf /var/lib/apt/lists/* && \
    pip install poetry

# nvflare requires 3.8.10
RUN conda create -n py3810 python=3.8.10

# Installing main dependencies
COPY pyproject.toml poetry.lock $FLYWHEEL/
RUN poetry env use /opt/conda/envs/py3810/bin/python && \
    poetry install --no-dev

# Installing the current project (most likely to change, above layer can be cached)
# Note: poetry requires a README.md to install the current project
COPY run.py manifest.json README.md $FLYWHEEL/
COPY fw_gear_nvflare_client $FLYWHEEL/fw_gear_nvflare_client
RUN poetry install --no-dev

## Configure entrypoint
RUN chmod a+x $FLYWHEEL/run.py
ENTRYPOINT ["poetry", "run", "python", "/flywheel/v0/run.py"]
