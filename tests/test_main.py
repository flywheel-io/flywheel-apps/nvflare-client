"""Module to test parser.py"""
import os
from pathlib import Path
from tempfile import TemporaryDirectory

from fw_gear_nvflare_client.main import format_output
from fw_gear_nvflare_client.utils import get_env


def test_format_output(tmpdir):
    tmpdir = Path(tmpdir)
    (tmpdir / "run_1").mkdir()
    (tmpdir / "run_2").mkdir()
    (tmpdir / "log.txt").touch()
    with TemporaryDirectory() as tmpdir2:
        tmpdir2 = Path(tmpdir2)
        format_output(tmpdir, tmpdir2)
        assert len(list(tmpdir2.glob("*.zip"))) == 2
        assert len(list(tmpdir2.glob("*.txt"))) == 1


def test_get_env():
    initial_len = len(os.environ)
    my_env = get_env(TEST=1)
    assert my_env["TEST"] == 1
    assert len(my_env) == initial_len + 1
