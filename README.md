

# NVflare client (nvflare-client)

[[_TOC_]]

## Overview

### Summary

This gear launches an NVIDIA FLARE (NVflare) client that can be used in a Federated 
Learning (FL) run. It takes as input an NVflare client ZIP archive that is created by 
the FL lead using the NVflare `provision` CLI. This ZIP contains the information to 
connect to the federated learning server which is responsible for sending NVFlare 
application and additional tasks executed by the client(s).

NVflare is a domain-agnostic, open-source, extensible SDK that allows researchers 
and data scientists to adapt existing ML/DL workflows (PyTorch, RAPIDS, Nemo, TensorFlow)
to a federated paradigm; and enables platform developers to build a secure, privacy 
preserving offering for a distributed multi-party collaboration.

Federated learning allows for multiple clients each with their own data to collaborate 
together without having to share their actual data. This allows for different parties 
located any place in the world to use their local secure protected data to perform 
tasks coordinated by an FL server set up in the secure cloud to achieve better learning 
results.

More information about NVFlare [here](https://nvidia.github.io/NVFlare/index.html).

### Classification
**Category:** analysis

**Gear Hierarchy Level**:  

- [X] Project
- [ ] Subject
- [ ] Session
- [ ] Acquisition
- [ ] Analysis

### Inputs

* **fed_client_zip** (File, Optional: `False`): The NVflare client zip file provided by the FL lead.

### Config

* **debug** (boolean, Default: `False`): Include debug statements in output.

### Outputs
* **log.txt**: The NVflare client log
* **Zip files**: One zip archive per Federated Learning run executed containing 
    the NVflare application and the local model weights. 

### Pre-requisites

* A ZIP archive containing the configuration details to connect to the FL server
  has been sent by the FL lead.
  
* Your Flywheel project has been curated according to the FL lead requirements

* For NVflare applications requiring GPU, Flywheel engine has been configured to
  run the `nvflare-client` on GPU enabled virtual machine. 
  
## Usage

### Workflow

```mermaid
sequenceDiagram
    actor user as Flyhweel User
    participant project as Flywheel Project
    participant client as Flywheel nvflare-client
    participant server as FL Server
    actor lead as FL Lead
    Note over lead: Develop NVflare app
    user->>project: Curate project 
    lead->>server: Start Server
    user->>client: Launch NVflare client gear
    Note over client: job starts
    client-->>server: Connect to server
    lead->>server: Deploy NVflare app
    lead->>server: Launch an FL run
    client-->>server: Request for task/app
    server-->>client: 
    client-->>project: Request data
    project-->>client: 
    Note over client: Model Training
    client-->>server: Send update
    lead->>server: Request model/logs
    server->>lead: 
    lead->>server: Shutdown client signal
    server-->>client: Shutdown client
    Note over client: job complete
    client-->>project: Store training run artifacts
```

The FL Lead is responsible for:
1) preparing the NVflare application  
2) provisioning the FL server 
3) provisioning each client and send them the ZIP artifact containing the configuration 
   details

In the workflow diagram only a single client is represented. Each client is 
responsible for curating their Flywheel project accordingly the requirement
imposed by the NVflare application designed by the FL lead. 

Once the project is curated and the server and NVflare app are ready, the client
launches the `nvflare-client` gear from the curated project using the provided
ZIP configuration archive. The client will connect to the server and wait for 
the FL run to start. Once the FL lead deploys the NVflare app and starts a new FL run, 
the client will execute the task (e.g. training) using the data available in the project
and send any update back to the server upon task completion. When deemed appropriate, 
the FL lead is responsible for shutting down the client, and the `nvflare-client` job
will complete, saving the local run artifacts in a Flywheel analysis container.

## Contributing

For more information about how to get started contributing to that gear,
checkout [CONTRIBUTING.md](CONTRIBUTING.md).

