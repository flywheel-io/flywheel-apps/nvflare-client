"""The utils module."""
import os


def get_api_key(config: dict) -> str:
    """Returns api-key value if present in config.json."""
    for inp in config["inputs"].values():
        if inp["base"] == "api-key" and inp["key"]:
            api_key = inp["key"]
            return api_key
    raise ValueError("no api-key found in config.json")


def get_env(**kwargs):
    """Returns all env vars extended by kwargs."""
    my_env = os.environ.copy()
    for k, v in kwargs.items():
        my_env[k] = v
    return my_env
