"""Main module."""
import logging
import shutil
import subprocess
from pathlib import Path

from flywheel_gear_toolkit.utils.zip_tools import zip_output

from fw_gear_nvflare_client.utils import get_env

log = logging.getLogger(__name__)


def start_client(client_dir: Path, project_lu: str, api_key: str) -> int:
    """Starts nvflare fed_client streaming log.

    Args:
        client_dir (Path): The root dir of the client.
        project_lu (str): Flywheel path (e.g. my-group/my-project).
        api_key (str): A Flywheel API key.

    Returns:
        (int): An error code.
    """
    my_env = get_env(FW_PROJECT=project_lu, FW_KEY=api_key)

    # removing & so that process don't get forked
    with open(client_dir / "startup" / "start.sh", "r") as fd:
        lines = fd.readlines()
        for i, l in enumerate(lines):
            if l.strip().endswith("sub_start.sh &"):
                lines[i] = l.replace("sub_start.sh &", "sub_start.sh")
    with open(client_dir / "startup" / "start_nofork.sh", "w") as fd:
        fd.writelines(lines)

    for shf in ["start.sh", "start_nofork.sh", "sub_start.sh"]:
        subprocess.call(["chmod", "+x", str(client_dir / "startup" / shf)])
    log.info("Launching NVflare client...")
    process = subprocess.Popen(
        [str(client_dir / "startup" / "start_nofork.sh")],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=True,
        env=my_env,
    )
    for stdout_line in iter(process.stdout.readline, b""):  # type: ignore
        log.info(stdout_line.decode())
    process.stdout.close()  # type: ignore
    return_code = process.wait()
    if return_code != 0:
        log.error(f"FL client exited with exit_code: {process.returncode}")
        return process.returncode
    else:
        return 0


def format_output(client_dir: Path, output_dir: Path):
    """Formats output and move to output_dir.

    Args:
        client_dir (Path): The root dir of the client.
        output_dir (Path): The path to the folder where the outputs are saved.
    """
    shutil.move(str(client_dir / "log.txt"), str(output_dir / "log.txt"))

    # zipping each runs
    runs = client_dir.glob("run*")
    for run in runs:
        zip_output(
            str(run.parent), str(run.name), str(output_dir / f"{str(run.name)}.zip")
        )
